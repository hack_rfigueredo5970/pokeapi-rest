// A function or hook to check if the user is authenticated
export function checkAuthentication() {
    // Retrieve the access token from local storage
    const accessToken = localStorage.getItem('accessToken');
    
    // Check if the access token exists and is not expired
    const isAuthenticated = !!accessToken;
  
    return isAuthenticated;
  }