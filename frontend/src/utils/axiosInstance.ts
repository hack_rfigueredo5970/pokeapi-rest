import axios from 'axios';
import { useNavigate } from 'react-router-dom'; 
// Create an Axios instance with custom headers
const axiosInstance = axios.create({
  baseURL: 'http://localhost:3000/api', // Replace with your API URL
});

// Add a request interceptor to set the access token if available in localStorage
axiosInstance.interceptors.request.use(
  (config) => {
    const accessToken = localStorage.getItem('accessToken');
    const refreshToken = localStorage.getItem('refreshToken'); 
    if (accessToken) {
      config.headers['Authorization'] = `Bearer ${accessToken}`;
    }

    if (refreshToken) {
        config.headers['Refresh-Token'] = refreshToken;
      }
      
    
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
    (response) => response,
    (error) => {
      if (error.response.status === 401) {
        localStorage.removeItem('accessToken')
        // Use useNavigate for redirection
        const navigate = useNavigate();
        navigate('/login');
      }
      return Promise.reject(error);
    }
  );

export default axiosInstance;
