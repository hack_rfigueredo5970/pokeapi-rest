import { Pokemon } from "./Pokemon";

export interface User {
    _id: string;
    username: string;
    pokemons: Pokemon[];
  }