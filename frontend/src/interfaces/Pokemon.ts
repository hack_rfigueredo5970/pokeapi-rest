export interface Pokemon {
    _id: string;
    name: string;
    height: number;
    weight: number;
    img: string;
    types: string[];
  }