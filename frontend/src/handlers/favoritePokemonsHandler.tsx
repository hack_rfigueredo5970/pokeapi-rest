import { Dispatch, SetStateAction } from 'react';
import axiosInstance from '../utils/axiosInstance';
import { Pokemon } from '../interfaces/Pokemon';

export const removeFavoritePokemon = async (userId: string, name: string, setFavoritePokemons: Dispatch<SetStateAction<Pokemon[]>>) => {
  try {
    const response = await axiosInstance.patch(`/user/favorites/remove/${userId}`, {name});
    setFavoritePokemons(() => [...response.data?.pokemons || []])
  } catch (error) {
    console.error('Error removing pokemon from favorites:', error);
  }
};


export const createFavoritePokemon = async (userId: string, formData: Partial<Pokemon>, setFavoritePokemons: Dispatch<SetStateAction<Pokemon[]>>) => {
    try {
  
      const response = await axiosInstance.post(`/user/favorites/${userId}`, formData);
      setFavoritePokemons(() => [...response.data?.pokemons || []]);
    } catch (error) {
      console.error('Error adding pokemon to favorites:', error);
    }
  };

  
  
