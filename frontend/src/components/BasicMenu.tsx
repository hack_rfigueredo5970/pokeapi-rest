import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { Link } from 'react-router-dom'; // Import Link from react-router-dom
import MenuIcon from '@mui/icons-material/Menu';

export const BasicMenu: React.FC<{ isAuthenticated: boolean }> = ({ isAuthenticated }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        <MenuIcon /> Menu
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {/* Use Link component to navigate to different routes */}
        {!isAuthenticated ? (
          [
            <MenuItem key="login" component={Link} to="/login" onClick={handleClose}>
              Login
            </MenuItem>,
            <MenuItem key="signup" component={Link} to="/signup" onClick={handleClose}>
              Sign Up
            </MenuItem>,
          ]
        ) : (
          [
            <MenuItem key="logout" component={Link} to="/logout" onClick={handleClose}>
              Log Out
            </MenuItem>,
            <MenuItem key="users" component={Link} to="/users" onClick={handleClose}>
              Users
            </MenuItem>,
          ]
        )}
      </Menu>
    </div>
  );
};
