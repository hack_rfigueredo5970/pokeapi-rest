import React, { useEffect, useState } from 'react';
import { Pokemon } from '../interfaces/Pokemon';
import axiosInstance from '../utils/axiosInstance';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { FixedSizeList, ListChildComponentProps } from 'react-window';
import Button from '@mui/material/Button';
import CreateFavoritePokemonForm from './CreateFavoritePokemonForm';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import IconButton from '@mui/material/IconButton';
import CancelIcon from '@mui/icons-material/Cancel';
import { removeFavoritePokemon } from '../handlers/favoritePokemonsHandler';
const FavoritePokemons: React.FC<{ userId: string }> = ({ userId }) => {
  const [favoritePokemons, setFavoritePokemons] = useState<Pokemon[]>([]);
  const [isCreatingFavoritePokemon, setIsCreatingFavoritePokemon] = useState(false); 
  useEffect(() => {
    // Fetch favorite Pokémon data for the user
    const fetchFavoritePokemons = async () => {
      try {
        const response = await axiosInstance.get(`/user/favorites/${userId}`);

        if (response.status === 200) {
          // Update the state with favorite Pokémon data
          setFavoritePokemons(response.data.pokemons);
        } else {
          console.error('Failed to fetch favorite Pokémon data');
        }
      } catch (error) {
        console.error('Failed to fetch favorite Pokémon data', error);
      }
    };

    // Call the fetch function when the component mounts
    fetchFavoritePokemons();
  }, [userId, isCreatingFavoritePokemon]);

  const handleDelete = async (name: string) => {
    await removeFavoritePokemon(userId, name, setFavoritePokemons);
  };
  
  const nameStyle: React.CSSProperties = {
    fontWeight: 'bold',
    fontSize: '1.2rem', // Adjust the font size as needed
  };
  // Define a custom row renderer for react-window
  const renderRow = (props: ListChildComponentProps) => {
    const { index, style } = props;
    const pokemon = favoritePokemons[index];

    return (
      <ListItem style={style} key={pokemon._id}>
            <img src={pokemon.img} alt={pokemon.name} />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => handleDelete(pokemon?.name || '')}
              >
                <CancelIcon />
              </IconButton>
        </ListItemSecondaryAction>
        <ListItemText
          primary={ <span style={nameStyle}> {pokemon.name}</span> }  
          secondary={`Height: ${pokemon.height}, Weight: ${pokemon.weight}, Type: ${pokemon.types.join(', ')}`}
        />
 
      </ListItem>
    );
  };

  return (
    <div>
      <h2>User Favorite Pokemons:</h2>
      {isCreatingFavoritePokemon ?  (
        <CreateFavoritePokemonForm  userId={userId} setFavoritePokemons={setFavoritePokemons} />
      ) :  (
    
        <Button
          variant="contained"
          color="primary"
          onClick={() => setIsCreatingFavoritePokemon(true)} // Call the form to create a task
        >
          Add Pokemon
        </Button>
          )}


      <div style={{ height: 400, width: '100%' }}>
        <List>
          <FixedSizeList
            height={400}
            width={'100%'}
            itemCount={favoritePokemons.length}
            itemSize={100} // Adjust the item size as needed
          >
            {renderRow}
          </FixedSizeList>
        </List>
      </div>
    </div>
  );
};

export default FavoritePokemons;
