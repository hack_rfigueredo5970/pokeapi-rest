import { Dispatch, SetStateAction, useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { createFavoritePokemon  } from '../handlers/favoritePokemonsHandler';
import { Pokemon } from '../interfaces/Pokemon';

interface CreateFavoritePokemonFormProps {
    userId: string;
    setFavoritePokemons: Dispatch<SetStateAction<Pokemon[]>>;// Define the onCancel prop// Define the setFavoritePokemons prop with correct type
}

function CreateFavoritePokemonForm({ userId, setFavoritePokemons  }: CreateFavoritePokemonFormProps) {
  const [open, setOpen] = useState(true);
  const [isNameEmpty, setIsNameEmpty] = useState(false);
  const [formData, setFormData] = useState({
    name: '',

  });

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
    if (name === 'name') {
        setIsNameEmpty(!value?.toString()?.trim());
    } 
  };

  const handleSubmit = () => {  
    if (!formData.name?.toString()?.trim() || isNameEmpty) {
        setIsNameEmpty(true);
        return
    } else  {
        setIsNameEmpty(false);
    }
    // Send a POST request to  add  pokemon to favorites
    createFavoritePokemon( userId, formData, setFavoritePokemons); // Pass formData and setFavoritePokemons to the createFavoritePokemon function
    handleClose();

// Reset the formData state
  setFormData({
    name: '',
  });
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleOpen}>
        Add Pokemon
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add Pokemon To User Favorites</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Fill in the details for the new task.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            name="name"
            label="Name"
            type="text"
            error={isNameEmpty} 
            helperText={isNameEmpty ? 'Name is required' : ''} 
            required 
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default CreateFavoritePokemonForm;
