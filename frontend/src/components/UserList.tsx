import React, { useEffect, useState } from 'react';
import { User } from '../interfaces/User';
import axiosInstance from '../utils/axiosInstance';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { FixedSizeList, ListChildComponentProps } from 'react-window';
import CatchingPokemonIcon from '@mui/icons-material/CatchingPokemon';
import ListItemIcon from '@mui/material/ListItemIcon';
import { useNavigate } from 'react-router-dom'; // Import useNavigate

const UserList: React.FC = () => {
  const [users, setUsers] = useState<User[]>([]);
  const navigate = useNavigate(); // Initialize useNavigate

  useEffect(() => {
    // Make an Axios GET request to fetch users from the /users endpoint
    axiosInstance
      .get('/users')
      .then((response) => {
        // Assuming the response contains an array of users
        setUsers(response.data.users);
      })
      .catch((error) => {
        console.error('Error fetching users:', error);
      });
  }, []);

  // Define a custom row renderer for react-window
  const renderRow = (props: ListChildComponentProps) => {
    const { index, style } = props;
    const user = users[index];

    const handleUserClick = (userId: string) => {
      const accessToken = localStorage.getItem('accessToken');
      const refreshToken = localStorage.getItem('refreshToken');
      const headers = {
        Authorization:`Bearer ${accessToken}`, 
        'Refresh-Token': refreshToken , 
      };

      navigate(`/user/favorite/${userId}`, { state: { headers } });
    };

    return (
      <ListItem
        style={style}
        key={user._id}
        component="div"
        disablePadding
        className="list-item"
        onClick={() => handleUserClick(user._id)} // Handle click here
      >
        <ListItemIcon>
          <CatchingPokemonIcon className='completed-icon' />
        </ListItemIcon>
        <ListItemText primary={user.username || 'test'} secondary={`numbers of pokemons: ${user?.pokemons?.length || 0}`} />
      </ListItem>
    );
  };

  return (
    <div>
      <h2>User List</h2>
      <div style={{ height: 600, width: '100wh' }}>
        <List>
          <FixedSizeList  
            height={400}
            width={'100%'}
            itemSize={50} // Adjust the item size as needed
            itemCount={users.length}
          >
            {renderRow}
          </FixedSizeList>
        </List>
      </div>
    </div>
  );
};

export default UserList;
