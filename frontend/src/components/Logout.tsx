import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const Logout: React.FC<{ setIsAuthenticated: (value: boolean) => void }> = ({ setIsAuthenticated })  => {
  const navigate = useNavigate();

  useEffect(() => {
    // Clear stored tokens (remove them from localStorage or your state management)
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    
    // Set isAuthenticated to false
    setIsAuthenticated(false);
    // Redirect to the login page
    navigate('/login');
  }, [navigate]);

  return <div>Logging out...</div>;
};

export default Logout;  
