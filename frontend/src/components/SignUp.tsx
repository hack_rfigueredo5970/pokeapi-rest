import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import axiosInstance from '../utils/axiosInstance';

const SignUp: React.FC = () => {
  const [formData, setFormData] = useState({
    username: '',
    password: '',
    passwordConfirmation: '',
  });

  const navigate = useNavigate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    console.debug({ name, value });
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      const response = await axiosInstance.post('/users/signup', formData);

      // Redirect the user to the desired route
      if (response.status === 200 || response.status === 201) {
        console.log('Sign up success');
        // You can handle successful sign-up logic here, such as displaying a success message.

        // After successful sign-up, you can navigate to the login page or any other route.
        navigate('/login');
      } else {
        console.error('Sign up failed:', response.status, response.data);
        // Handle sign-up failure, such as displaying an error message to the user.
      }
    } catch (error) {
      console.error('Sign up failed:', error);
      // Handle sign-up error, such as displaying an error message to the user.
    }
  };

  return (
    <div>
      <h2>Sign Up</h2>
      <Box
        component="form"
        onSubmit={handleSubmit} // Move the onSubmit handler to the Box component
        sx={{
          '& .MuiTextField-root': { m: 1, width: '100%' },
        }}
        noValidate
        autoComplete="on"
      >
        <div>
          <TextField
            required
            id="username"
            name="username"
            label="Username"
            variant="outlined"
            value={formData.username}
            onChange={handleChange}
          />
        </div>
        <div>
          <TextField
            required
            id="password"
            name="password"
            label="Create Password"
            type="password"
            variant="outlined"
            value={formData.password}
            onChange={handleChange}
          />
        </div>
        <div>
          <TextField
            required
            id="passwordConfirmation"
            name="passwordConfirmation"
            label="Confirm Password"
            type="password"
            variant="outlined"
            value={formData.passwordConfirmation}
            onChange={handleChange}
          />
        </div>
        <button type="submit">Sign Up</button>
      </Box>
    </div>
  );
};

export default SignUp;
