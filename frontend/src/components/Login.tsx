import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import TextField from '@mui/material/TextField'; // Import TextField from Material-UI
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import axios from 'axios';


const Login: React.FC<{ setIsAuthenticated: (value: boolean) => void }> = ({ setIsAuthenticated }) => {
  const [formData, setFormData] = useState({
    username: '',
    password: '',
  });

  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const navigate = useNavigate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      const response = await axios.post('/users/login', formData);
      console.log(response)
      // Redirect the user to the desired route
      if (response.status === 200) {
        console.log('Login success');
        // Handle successful login
        const { accessToken, refreshToken } = response.data;

        // Store tokens in localStorage
        localStorage.setItem('accessToken', accessToken);
        localStorage.setItem('refreshToken', refreshToken);

        // Set isAuthenticated to true
        setIsAuthenticated(true);

        // Redirect to the /users route
        navigate('/users');
      } else {
        setIsAuthenticated(false);
        console.error('Login failed:', response.data);
        setErrorMessage( `Login failed with error ${response.status}:  ${response.data} `);
      }
    } catch (error) {
      console.error(`Login failed:  ${(error as Error)?.message} `);
      setIsAuthenticated(false);
      setErrorMessage( `Login failed with error: ${(error as Error)?.message}`);
    }
  };

  return (
    <div>
      {errorMessage && ( <Alert severity="error"> 
          {errorMessage}
        </Alert>)} 
      <h2>Login</h2>
      
      <Box
        component="form"
        onSubmit={handleSubmit} // Move the onSubmit handler to the Box component
      >
        <div>
          <TextField
            label="Username"
            id="username"
            name="username"
            value={formData.username}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <TextField
            label="Password"
            type="password"
            id="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            required
          />
        </div>
        <button type="submit">Submit</button>
      </Box>
    </div>
  );
};

export default Login;
