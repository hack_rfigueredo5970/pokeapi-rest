import { useState } from 'react';
import { BrowserRouter as Router, Route,  Routes, Navigate, Outlet, useParams } from 'react-router-dom'; // Import Routes and BrowserRouter
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import axios from 'axios';

import './App.css'
import UsersList from './components/UserList';
import NotFound from './components/NotFound';
import Login from './components/Login';
import Logout from './components/Logout';
import {BasicMenu} from './components/BasicMenu';
import SignUp from './components/SignUp';
import FavoritePokemons from './components/FavoritePokemons';


const queryClient = new QueryClient();

// Set your API base URL for axios
axios.defaults.baseURL = 'http://localhost:3000/api';


const App: React.FC = () =>  {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  return (
    <QueryClientProvider client={queryClient}>
    
    <Router>
    <div className="App">
      <header className="global-header">
          <h1>Favorites Pokemon</h1>
          <nav>
            <BasicMenu isAuthenticated={isAuthenticated} />
          </nav>
        </header> 
        <Routes>
           <Route path="/login" element={<Login setIsAuthenticated={setIsAuthenticated} />} />
           
           <Route path="/signup" element={<SignUp />} /> 
            
            <Route path="/not-found" element={<NotFound />} />

          <Route path="/logout" element={<Logout setIsAuthenticated={setIsAuthenticated} />} />

          {/* Private routes requiring authentication */}
          <Route path="/users" element={<PrivateRoute isAuthenticated={isAuthenticated} />}>
            <Route index element={<UsersList />} /> 
          </Route> 

          <Route path="/user/favorite/:userId" element={<PrivateRoute isAuthenticated={isAuthenticated} />}>
            <Route index element={<FavoritePokemonsWrapper />} /> 
          </Route> 

          {/* Redirect to the user page if no route matches * */}
          <Route index element={<Navigate to="/login" />} />

          {/* Redirect to the not-found page for any unknown route */}
          <Route path="*" element={<NotFound />} />
        </Routes>
    </div>
    </Router>
    <ReactQueryDevtools />
    </QueryClientProvider>
  );
}

const PrivateRoute: React.FC<{ isAuthenticated: boolean }> = ({ isAuthenticated }) => {
  if (!isAuthenticated) {
    // Redirect to login if not authenticated
    return <Navigate to="/login" />;
  }

  return <Outlet />;
}

function FavoritePokemonsWrapper() {
  const { userId } = useParams(); // Get the userId from route parameters
  
  if (!userId) {
    // You can redirect to an error page or display a message here
    return <div>User ID not found</div>;
  }
  return <FavoritePokemons userId={userId} />;
}


export default App;
