import { MongoClient } from 'mongodb';
import { BcryptHashingService }  from './users/application/services/hashing/BcryptHashingService'; // Import your BcryptHashingService
import { config } from './config/config';


async function resetMongoDB() {
  const url = config.database.mongo.MONGO_DB_URI; // MongoDB connection URL from config
  const dbName = config.database.mongo.DATABASE_NAME; // Database name from config
  
  const hashingService = new BcryptHashingService(); // Create an instance of your hashing service
  
  let client; // Declare the client variable for dropping the database
  let closeClient; // Declare another client variable for closing the connection

  try {
    // Create a new client instance for dropping the database
    client = new MongoClient(url);

    // Connect to MongoDB for dropping the database
    await client.connect();

    // Drop the existing database if it exists
    await client.db(dbName).dropDatabase();

    // Create a new client instance for closing the connection
    closeClient = new MongoClient(url);

    // Connect to MongoDB for closing the connection
    await closeClient.connect();

    // Close any existing connection using the same port
    const adminDb = closeClient.db('admin');
    const existingConnections = await adminDb.command({ listDatabases: 1 });
    for (const dbInfo of existingConnections.databases) {
      if (dbInfo.name === dbName) {
        console.log(`Closing existing connection to "${dbName}"...`);
        await closeClient.close(); // Close the existing connection
        break;
      }
    }

    // Create a new database
    const db = client.db(dbName);

    // Create "User" collection
    await db.createCollection('User');

    // Hash the password before storing it
    const hashedPassword = await hashingService.hashPassword('123'); // Hash the password

    // Insert a new document into the "User" collection
    const userCollection = db.collection('User');
    const newUser = {
      username: 'test',
      password: hashedPassword, // Store the hashed password
    };
    await userCollection.insertOne(newUser);

    // Create "Pokemon" collection
    await db.createCollection('Pokemon');

    console.log(`MongoDB database "${dbName}" reset successfully.`);
  } catch (error) {
    console.error('Error resetting MongoDB database:', error);
  } finally {
    if (client) {
      // Close the MongoClient for dropping the database
      await client.close();
    }
    if (closeClient) {
      // Close the MongoClient for closing the connection
      await closeClient.close();
    }
  }
}
  
// Call the resetMongoDB function
resetMongoDB();
  