import { Request, Response, NextFunction } from 'express';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { config } from '../config/config'; // Import your configuration
import { UnauthorizedError } from '../errors/errors';
import { generateAccessToken } from '../users/application/services/authentication/AuthenticationService';

export interface CustomRequest extends Request {
  token: string | JwtPayload;
}

export const authenticate = (req: Request, res: Response, next: NextFunction) => {
  const token: string = req.header('Authorization')?.replace('Bearer ', '') as string;
  const refreshToken: string = req.header('Refresh-Token') as string;

  try {
    if (!token) {
      throw new UnauthorizedError();
    }

    try {
      const decoded = jwt.verify(token || '', config.jwtSecret || '');
      if (decoded) {  
        (req as CustomRequest).token = decoded;
      }

      return next();
    } catch (accessTokenError) {

      if (!refreshToken) {
        throw new UnauthorizedError('Access token is invalid or expired');
      } else  {
        try {
          console.log('Refreshing Access Token...');
          const decodedRefreshToken = jwt.verify(refreshToken, config.refreshTokenSecret || '') as Record<string, string>;
  
          if (decodedRefreshToken) {
            // Generate a new access token and send it in the response
            const newAccessToken = generateAccessToken(decodedRefreshToken.userId); 
            // Implement your access token generation function
            const decoded = jwt.verify(newAccessToken || '', config.jwtSecret || '');
            if (decoded) {  
              (req as CustomRequest).token = decoded;
            }
            return next(); // Continue with the request
          }
        } catch (refreshTokenError) {
          throw new UnauthorizedError('Refresh Token is invalid or expired');
        }
      }
    }

    throw new UnauthorizedError('Authentication failed');

  } catch (error) {
    next(error);
  }

  return;
};
