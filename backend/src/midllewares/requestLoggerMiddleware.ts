import { Request, Response, NextFunction } from 'express';

// logs the incoming requests
export function requestLoggerMiddleware(req: Request, res: Response, next: NextFunction) {
  console.log(`[${new Date().toISOString()}] ${req.method} ${req.url}`);
  next();
}