
import { NextFunction, Request, Response } from 'express';
import { 
  NotFoundError, 
  BadRequestError, 
  InternalServerError, 
  DatabaseConnectionError, 
  UnauthorizedError, 
  DatabaseError, 
  PokemonAPIError, 
  ConflictError, 
} from '../errors/errors';

export const errorHandlerMiddleware = (
  error: Error,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  if (error instanceof NotFoundError) {
    res.status(404).json({ error: error.message });
  } else if (error instanceof BadRequestError) {
    res.status(400).json({ error: error.message });
  } else if (error instanceof UnauthorizedError) {
    res.status(401).json({ error: error.message });
  } else if (error instanceof ConflictError) {
    res.status(409).json({ error: error.message });
  } else if (error instanceof InternalServerError) {
    res.status(500).json({ error: error.message });
  } else if (error instanceof PokemonAPIError) {
    res.status(500).json({ error: error.message });
  } else if (error instanceof DatabaseConnectionError) {
    res.status(500).json({ error: error.message });
  } else if (error instanceof DatabaseError) {
    res.status(500).json({ error: error.message });
  } else {
    console.error('An error occurred:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
  next();
};