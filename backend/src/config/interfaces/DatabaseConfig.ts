import { DatabaseType } from '../types/DatabaseTypeEnum';


export interface DatabaseConfig {
  type: DatabaseType;
  uri?: string; // MongoDB
  username?: string; // MongoDB Atlas username
  password?: string; // MongoDB Atlas password
  host?: string;
  port?: number;
  dbName?: string;
}
  