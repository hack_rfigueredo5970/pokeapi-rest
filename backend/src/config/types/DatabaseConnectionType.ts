import { Db } from 'mongodb';
import { Connection } from 'mongoose';

export type DatabaseConnectionType = Db | null | Connection;