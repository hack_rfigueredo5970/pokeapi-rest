// Add other database types as needed

export enum DatabaseTypeEnum {
  MONGODB = 'mongodb',
  MONGOOSE = 'mongoose',
}


export type DatabaseType = `${DatabaseTypeEnum}`;
  