/* eslint-disable import/no-extraneous-dependencies */
import { DatabaseType, DatabaseTypeEnum } from './types/DatabaseTypeEnum';

import { MongoClient, Db,  ServerApiVersion  } from 'mongodb';
import { config } from './config';
import { DatabaseConfig } from './interfaces/DatabaseConfig';
import { DatabaseConnectionError } from '../errors/errors';
// Define configuration options for different databases

export type DatabaseConnectionType = Db;
let db:  DatabaseConnectionType; // Initialize the database connection

export async function connectToDatabase(): Promise<DatabaseConnectionType> {
  if (!db) {
    const databaseType: DatabaseType  =  config.database.type as DatabaseType;

    if (databaseType === DatabaseTypeEnum.MONGODB) {

      const mongoConfig: DatabaseConfig  = {
        type: databaseType,
        uri: config.database.mongo.MONGO_DB_URI,
        dbName: config.database.mongo.DATABASE_NAME,
      };

      const uri: string  = mongoConfig.uri as string;

      if (!uri) {
        throw new DatabaseConnectionError('Missing MONGO_DB_URI env variable');
      }
      
      const client = new MongoClient(uri, {
        serverApi: {
          version: ServerApiVersion.v1,
          strict: true,
          deprecationErrors: true,
        },
      });
      // Connect the client to the server	(optional starting in v4.7)
      await client.connect().catch(e => {
        const errorMessage: string  = `Connection error ${e.message}`;
        console.error(errorMessage);
        throw new DatabaseConnectionError(errorMessage);
      });

      db = client.db(mongoConfig.dbName);
      console.log(`Connected to database "${mongoConfig.dbName}" in MongoDB!`);
    } else {
      throw new Error('Unsupported database type');
    }

  
  }
  return db;
}