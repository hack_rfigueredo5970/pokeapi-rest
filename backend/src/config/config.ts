import dotenv from 'dotenv';

dotenv.config(); // Load environment variables from a .env file

export const config = {
  database: {
    mongo: {
      MONGO_DB_URI: process.env.MONGO_DB_URI || 'mongodb://localhost/localdb',
      DATABASE_NAME: process.env.DB_NAME || 'pokemonDb',
    },
    type: process.env.DB_TYPE,
  },
  server: {
    port: Number(process.env.PORT) || 3000,
  },
  jwtSecret: process.env.JWT_SECRET,
  refreshTokenSecret: process.env.REFRESH_TOKEN_SECRET,
};
