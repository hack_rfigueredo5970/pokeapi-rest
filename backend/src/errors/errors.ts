export class NotFoundError extends Error {
  constructor(message: string = 'Not Found') {
    super(message);
    this.name = 'NotFoundError';
  }
}

export class ConflictError extends Error {
  constructor(message: string = 'Conflict') {
    super(message);
    this.name = 'ConflictError';
  }
}



export class BadRequestError extends Error {
  constructor(message: string = 'Bad Request') {
    super(message);
    this.name = 'BadRequestError';
  }
}

export class InternalServerError extends Error {
  constructor(message: string = 'Internal Server Error') {
    super(message);
    this.name = 'InternalServerError';
  }
}

export class PokemonAPIError extends Error {
  constructor(message: string = 'Pokemon  API Error') {
    super(message);
    this.name = 'PokemonAPIError';
  }
}

export class DatabaseConnectionError extends Error {
  constructor(message: string = 'Database connection failed') {
    super(message);
    this.name = 'DatabaseConnectionError';
  }
}

export class DatabaseError extends Error {
  constructor(message: string = 'Database Error') {
    super(message);
    this.name = 'DatabaseError';
  }
}

export class UnauthorizedError  extends Error {
  constructor(message: string = 'Unauthorized') {
    super(message);
    this.name = 'UnauthorizedErrorr';
  }
}