import express, { Express  } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

// load enviromental variables
import './config/load_env_vars';
import { config } from './config/config';
import { requestLoggerMiddleware } from './midllewares/requestLoggerMiddleware';
import { errorHandlerMiddleware } from './midllewares/errorHandlerMiddleware';
import { createRoutes } from './routes';
async function boostrap() {
  const app: Express = express();
  const port: number = config.server.port;
  
  // Enable CORS
  app.use(cors());
  app.use(bodyParser.json());
  app.use(requestLoggerMiddleware);
  

  
  const routes = await createRoutes();
  app.use('/api', routes);

  app.use(errorHandlerMiddleware);



  app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
  }); 
  
}

boostrap().then().catch();