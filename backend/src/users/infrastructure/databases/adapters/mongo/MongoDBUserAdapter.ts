import { DatabaseConnectionError, DatabaseError } from '../../../../../errors/errors';
import { UserRepository } from '../../../../domain/repositories/UserRepository';
import { User } from '../../../../domain/entities/interfaces/User';
import { Collection, Db, ObjectId, OptionalId } from 'mongodb';
import { DatabaseConnectionType } from '../../../../../config/types/DatabaseConnectionType';
import { UserDocument } from './interfaces/UserDocument';
import { Pokemon } from '../../../../../pokemons/domain/entities/interfaces/Pokemon';

class MongoDBUserAdapter implements UserRepository {

  private collection: Collection;

  private db : DatabaseConnectionType; 

  constructor(db: DatabaseConnectionType) {
    if (!db) 
      throw new DatabaseConnectionError();
    this.collection = (db as Db).collection('User');
    this.db = db;
  }
  
  async createUser(user: User): Promise<User> {
    try {
      // Create a MongoDB document from the User object
      const userDoc: OptionalId<UserDocument>  = {
        username: user.username,
        password: user.password,
        token: user.token,
      };

      const result = await this.collection.insertOne(userDoc);
      if (!result.insertedId) {
        throw new DatabaseError('Failed to insert user');
      }
  
      return (this.collection.findOne({ _id: result.insertedId })  as unknown as User);
  
    } catch (error) {
      throw new DatabaseError(`Failed to create user: ${(error as Error).message}`);
    }
  }

  async updateUser(id: string, updatedUserData: Partial<User>): Promise<User | null> {
    try {
      const result = await this.collection.updateOne({ _id: new ObjectId(id) }, { $set: updatedUserData });
  
      if (result.modifiedCount === 0) {
        return null; // No user was updated
      }
  
      const updatedUser = await this.collection.findOne({ _id: new ObjectId(id) });
      return updatedUser as unknown as User;
    } catch (error) {
      throw new DatabaseError(`Failed to update user: ${(error as Error).message}`);
    }
  }


  async getAllUser(): Promise<User[]> {
    try {
      const users = await this.collection.find({}, { projection: { password: 0, token: 0 } }).toArray();
      return users.map((user) => user as unknown as User);
    } catch (error) {
      throw new DatabaseError(`Failed to retrieve all users: ${(error as Error).message}`);
    }
  }

  async findByUserName(username: string): Promise<User | null> {
    try {
      return ( await await this.collection.findOne({ username }) as unknown as User );
    } catch (error) {
      throw new DatabaseError(`Failed to find user by username: ${(error as Error).message}`);
    }
  }

  async isUserNameUnique(username: string): Promise<boolean> {
    const user = await this.collection.findOne({ username });
    return !user; // Return true if username is unique (user doesn't exist)
  }

  async findById(id: string): Promise<User | null> {
    try {
      return (this.collection.findOne({ _id: new ObjectId(id) }) as unknown as User);
    } catch (error) {
      throw new DatabaseError(`Failed to get user by ID: ${(error as Error).message}`);
    }
  }

  async addPokemonToUser(userId: string, pokemon: Pokemon): Promise<User | null> {
    try {
      const result = await this.collection.updateOne(
        { _id: new ObjectId(userId) },
        { $push: { pokemons: pokemon } },
      );
  
      if (result.modifiedCount === 0) {
        return null; // No user was updated
      }
  
      const updatedUser = await this.collection.findOne({ _id: new ObjectId(userId) });
      return updatedUser as unknown as User;
    } catch (error) {
      throw new DatabaseError(`Failed to add Pokémon to user: ${(error as Error).message}`);
    }
  }
  
  async getUserPokemons(userId: string): Promise<Pokemon[]> {
    try {
      const user = await this.collection.findOne({ _id: new ObjectId(userId) });
      return user?.pokemons || [];
    } catch (error) {
      console.error(`Failed to retrieve user's Pokémon: ${(error as Error).message}`);
      return [];
    }
  }
  
  async removePokemonFromUser(userId: string, pokemonId: string): Promise<User | null> {
    try {
      const result = await this.collection.updateOne(
        { _id: new ObjectId(userId) },
        { $pull: { pokemons: { _id: new ObjectId(pokemonId) } } },
      );
  
      if (result.modifiedCount === 0) {
        return null; // No user was updated
      }
  
      const updatedUser = await this.collection.findOne({ _id: new ObjectId(userId) });
      return updatedUser as unknown as User;
    } catch (error) {
      throw new DatabaseError(`Failed to remove Pokémon from user: ${(error as Error).message}`);
    }
  }

  async removePokemonFromAllUsers(pokemonId: string): Promise<void> {
    try {
      await (this.db as Db)?.command({
        update: this.collection.collectionName,
        updates: [
          {
            q: { 'pokemons._id': new ObjectId(pokemonId) },
            u: { $pull: { pokemons: { _id: new ObjectId(pokemonId) } } },
            multi: true,
          },
        ],
      });
    } catch (error) {
      throw new DatabaseError(`Failed to remove Pokémon from all users: ${(error as Error).message}`);
    }
  }

  
}

export { MongoDBUserAdapter };
