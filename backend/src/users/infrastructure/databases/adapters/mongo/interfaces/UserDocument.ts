import { User } from '../../../../../domain/entities/interfaces/User';


export interface UserDocument extends Omit<User, '_id'> {}

