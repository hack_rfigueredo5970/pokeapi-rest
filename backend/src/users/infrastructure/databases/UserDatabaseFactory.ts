import { MongoDBUserAdapter } from './adapters/mongo/MongoDBUserAdapter';
import { DatabaseType, DatabaseTypeEnum } from '../../../config/types/DatabaseTypeEnum';
import { UserRepository } from '../../domain/repositories/UserRepository';
import { config } from '../../../config/config';
import { DatabaseConnectionType, connectToDatabase } from '../../../config/db';

class UserDatabaseFactory {
  static async createUserAdapter(): Promise<UserRepository> {
    const databaseType: DatabaseType  =  config.database.type as DatabaseType;
    if (databaseType === DatabaseTypeEnum.MONGODB ) {
      const db: DatabaseConnectionType = await connectToDatabase();
      return new MongoDBUserAdapter(db);
    } else {
      throw new Error(`Unsupported database type: ${databaseType}`);
    }
  }
}

export { UserDatabaseFactory };
