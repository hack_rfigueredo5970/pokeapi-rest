import { BcryptHashingService } from '../application/services/hashing/BcryptHashingService';
import { HashingService } from '../application/services/hashing/interfaces/HashingService';
import { PokemonAPIService } from '../../pokemons/application/services/interfaces/PokemonAPIService';
import { PokemonAPIServiceAdapter } from '../../pokemons/application/services/PokemonAPIServiceAdapter';
import { AuthenticateUserUseCase } from '../application/use-cases/AuthenticateUserUSeCase';
import { CreateUserUseCase } from '../application/use-cases/CreateUserUseCase';
import { FindUserByUserNameUseCase } from '../application/use-cases/FindUserByUserNameUseCase';
import { FindUserByIdUseCase } from '../application/use-cases/FindUserByIdUseCase';
import { UserRepository } from '../domain/repositories/UserRepository';
import { UserDatabaseFactory } from './databases/UserDatabaseFactory';
import { UserController } from './http/controllers/UserController';
import { RefreshTokenUseCase } from '../application/use-cases/RefreshTokenUseCase';
import { GetAllUserUseCase } from '../application/use-cases/GetAllUserUseCase';
import { GetUserPokemonsUseCase } from '../application/use-cases/GetUserPokemonsUseCase';
import { AddPokemonToUserUseCase } from '../application/use-cases/AddPokemonToUserUseCase';
import { PokemonRepository } from '../../pokemons/domain/repositories/PokemonRepository';
import { PokemonDatabaseFactory } from '../../pokemons/infrastructure/databases/PokemonDatabaseFactory';
import { CreatePokemonUseCase } from '../../pokemons/application/use-cases/CreatePokemonUseCase';
import { RemovePokemonFromUserUseCase } from '../application/use-cases/RemovePokemonFromUserUseCase';
  
// Create use cases and controllers, injecting the required dependencies
export const initializeUserController = async (): Promise<UserController> => {
  const hashingService: HashingService =  new BcryptHashingService();
  const pokemonAPIService: PokemonAPIService = new PokemonAPIServiceAdapter();
  const userRepository: UserRepository = await UserDatabaseFactory.createUserAdapter();
  const pokemonRepository: PokemonRepository = await PokemonDatabaseFactory.createPokemonAdapter();

  // use cases
  const createUserUseCase = new CreateUserUseCase(userRepository, hashingService);
  const getAllUserUseCase = new GetAllUserUseCase(userRepository);
  const findUserByUserNameUseCase = new FindUserByUserNameUseCase(userRepository);
  const findUserByIdUseCase = new FindUserByIdUseCase(userRepository);
  const authenticateUserUseCase = new AuthenticateUserUseCase(userRepository, hashingService);
  const refreshTokenUseCase = new RefreshTokenUseCase();
  const getUserPokemonsUseCase = new GetUserPokemonsUseCase(userRepository);
  const createPokemonUseCase = new CreatePokemonUseCase(pokemonRepository, pokemonAPIService);
  const removePokemonFromUserUseCase = new RemovePokemonFromUserUseCase(userRepository, pokemonRepository);
  
  const addPokemonToUserUseCase = new AddPokemonToUserUseCase( 
    userRepository, 
    pokemonRepository, 
    createPokemonUseCase,
  );

  return new UserController(
    createUserUseCase, 
    getAllUserUseCase, 
    findUserByUserNameUseCase, 
    findUserByIdUseCase, 
    authenticateUserUseCase, 
    refreshTokenUseCase, 
    getUserPokemonsUseCase,
    addPokemonToUserUseCase,
    removePokemonFromUserUseCase,
  );
};

