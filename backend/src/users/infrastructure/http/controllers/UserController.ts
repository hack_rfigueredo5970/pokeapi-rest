
// libs
import { BadRequestError, NotFoundError, UnauthorizedError } from '../../../../errors/errors';
import { Request, Response, NextFunction } from 'express';

// use cases
import { CreateUserUseCase } from '../../../application/use-cases/CreateUserUseCase';
import { FindUserByUserNameUseCase } from '../../../application/use-cases/FindUserByUserNameUseCase';
import { FindUserByIdUseCase } from '../../../application/use-cases/FindUserByIdUseCase';
import { AuthenticateUserUseCase } from '../../../application/use-cases/AuthenticateUserUSeCase';
import { RefreshTokenUseCase } from '../../../application/use-cases/RefreshTokenUseCase';
import { GetAllUserUseCase } from '../../../application/use-cases/GetAllUserUseCase';
import { GetUserPokemonsUseCase } from '../../../application/use-cases/GetUserPokemonsUseCase';
import { AddPokemonToUserUseCase } from '../../../application/use-cases/AddPokemonToUserUseCase';
import { RemovePokemonFromUserUseCase } from '../../../application/use-cases/RemovePokemonFromUserUseCase';

// interfaces
import { User } from '../../../domain/entities/interfaces/User';
import { Pokemon } from '../../../../pokemons/domain/entities/interfaces/Pokemon';

// middlewares
import { CustomRequest } from '../../../../midllewares/authenticationMiddleware';
import { AuthTokenResponse } from '../../../../utils/interfaces/AuthTokenResponse';



export class UserController {
  constructor(
    private readonly createUserUseCase: CreateUserUseCase,
    private readonly getAllUserUseCase: GetAllUserUseCase,
    private readonly findUserByUserNameUseCase: FindUserByUserNameUseCase,
    private readonly findUserByIdUseCase: FindUserByIdUseCase,
    private readonly authenticateUserUseCase: AuthenticateUserUseCase,
    private readonly refreshTokenUseCase: RefreshTokenUseCase,
    private readonly getUSerPokemonsUseCase: GetUserPokemonsUseCase,
    private readonly addPokemonToUserUseCase: AddPokemonToUserUseCase,
    private readonly removePokemonFromUserUseCase: RemovePokemonFromUserUseCase,
  ) {}

  async signUp(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const { username, password, passwordConfirmation } = req.body;
    
      // Check if any required attributes are missing
      if (!username) {
        throw new BadRequestError('Missing required field: username');
      }
      if (!password) {
        throw new BadRequestError('Missing required field: password ');
      }
      if (!passwordConfirmation) {
        throw new BadRequestError('Missing required field: Missing: passwordConfirmation');
      }
      // Check if password and passwordConfirmation match
      if (password !== passwordConfirmation) {
        throw new BadRequestError('Confirmation password do not match.');
      }
      const createdUser = await this.createUserUseCase.execute({ username, password  });
      delete createdUser?.password;
      delete createdUser?.pokemons;
      res.status(201).json(createdUser);
    } catch (error) {
      next(error);
    }
    return;
  }

  async getAllUser(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const users : User[] = await this.getAllUserUseCase.execute() || [];
      res.status(200).json({ users });
    } catch (error) {
      next(error);
    }
    return;
  }

  async findByUserName(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const { username } = req.params;
      const user : User | null = await this.findUserByUserNameUseCase.execute(username);
    
    
      if (!user) {
        throw new NotFoundError('User not found'); 
      }

      delete user?.password;
      delete user?.pokemons;

      res.status(200).json(user);
    } catch (error) {
      next(error);
    }
    return;
  }

  async findById(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const token =  (req as CustomRequest).token;

      if (!token) {
        throw new UnauthorizedError('Invalid Access Token'); 
      }
    
      const  { userId } = token as unknown as { [key:string]: string };
      const user : User | null = await this.findUserByIdUseCase.execute(userId);
      
      if (!user) {
        throw new NotFoundError('User not found'); 
      }
      delete user.password;
      delete user.pokemons;
      res.status(200).json(user);
    } catch (error) {
      next(error);
    }
    return;
  }

  async login(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const { username, password } = req.body;

      const { accessToken, refreshToken }: AuthTokenResponse = await this.authenticateUserUseCase.execute(username, password);

      if (!accessToken)
        throw new UnauthorizedError('Invalid credentials'); 

      res.status(200).json({ accessToken, refreshToken });

    } catch (error) {
      next(error);
    }
    return;
  }

  async refreshToken(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const token: string = req.body.refreshToken  as string;
      const { accessToken, refreshToken }: AuthTokenResponse = await this.refreshTokenUseCase.execute(token);
      if (!accessToken)
        throw new UnauthorizedError('Invalid credentials'); 
      res.status(200).json({ accessToken, refreshToken });
    } catch (error) {
      next(error);
    }
    return;
  }

  async getUserPokemons(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const { iduser } = req.params;
      const pokemons : Pokemon[] = await this.getUSerPokemonsUseCase.execute(iduser) || [];
      res.status(200).json({ pokemons });
    } catch (error) {
      next(error);
    }
    return;
  }

  async addPokemonToUser(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const { iduser } = req.params;
      const { name } = req.body;
      const user: User = await this.addPokemonToUserUseCase.execute(iduser, name) as User;

      const pokemons : Pokemon[] = await this.getUSerPokemonsUseCase.execute(user?._id || '') || [];
      
      res.status(200).json({ pokemons });
    } catch (error) {
      next(error);
    }
    return;
  }


  async removePokemonFromUser(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const { iduser } = req.params;
      const { name } = req.body;
      const user: User = await this.removePokemonFromUserUseCase.execute(iduser, name) as User;

      const pokemons : Pokemon[] = await this.getUSerPokemonsUseCase.execute(user?._id || '') || [];
      
      res.status(200).json({ pokemons });
    } catch (error) {
      next(error);
    }
    return;
  }

}