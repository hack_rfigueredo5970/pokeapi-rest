import { Router } from 'express';
import { initializeUserController } from '../../dependencies';
import { authenticate } from '../../../../midllewares/authenticationMiddleware';

const initializeUserFavoritesRouter = async () => {

  const userController = await initializeUserController();
  const userFavoritesRouter = Router();
  userFavoritesRouter.get('/:iduser/', authenticate, userController.getUserPokemons.bind(userController));
  userFavoritesRouter.post('/:iduser/', authenticate, userController.addPokemonToUser.bind(userController));
  userFavoritesRouter.patch('/remove/:iduser', authenticate, userController.removePokemonFromUser.bind(userController));
  return userFavoritesRouter;
};

export default initializeUserFavoritesRouter;
