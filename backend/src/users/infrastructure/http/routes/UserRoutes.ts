import { Router } from 'express';
import { initializeUserController } from '../../dependencies';
import { authenticate } from '../../../../midllewares/authenticationMiddleware';

const initializeUserRouter = async () => {

  const userController = await initializeUserController();
  const userRouter = Router();

  userRouter.get('/', authenticate, userController.getAllUser.bind(userController));
  userRouter.get('/user', authenticate, userController.findById.bind(userController));
  userRouter.get('/:username', authenticate, userController.findByUserName.bind(userController));
  userRouter.post('/login', userController.login.bind(userController));
  userRouter.post('/signup', userController.signUp.bind(userController));
  userRouter.post('/refresh-token', userController.refreshToken.bind(userController));
  
  return userRouter;
};

export default initializeUserRouter;
