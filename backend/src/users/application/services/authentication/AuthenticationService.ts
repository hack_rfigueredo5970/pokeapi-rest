import jwt from 'jsonwebtoken';
import { config } from '../../../../config/config'; // Import your configuration

export const generateAccessToken = (userId: string) => {
  return jwt.sign({ userId }, config.jwtSecret || '', { expiresIn: '1h' });
};

export const generateRefreshToken = (userId: string) => {
  const refreshToken = jwt.sign({ userId }, config.refreshTokenSecret || '', { expiresIn: '7d' }); // Set an appropriate expiration time
  return refreshToken;
};
