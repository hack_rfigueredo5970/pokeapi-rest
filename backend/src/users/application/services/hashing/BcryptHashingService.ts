import * as bcrypt from 'bcrypt';
import { HashingService } from './interfaces/HashingService';

export class BcryptHashingService implements HashingService {
  async hashPassword(password: string): Promise<string> {
    const saltRounds = 10; // You can adjust this value as needed

    const hashedPassword = await bcrypt.hash(password, saltRounds);
    return hashedPassword;
  }

  async comparePassword(password: string, hashedPassword: string): Promise<boolean> {
    return bcrypt.compare(password, hashedPassword);
  }
}
