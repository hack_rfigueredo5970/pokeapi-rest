import { UserRepository } from '../../domain/repositories/UserRepository';
import { Pokemon } from '../../../pokemons/domain/entities/interfaces/Pokemon';

export class GetUserPokemonsUseCase {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(userId:string): Promise<Pokemon[]> {
    return this.userRepository.getUserPokemons(userId);
  }
}
