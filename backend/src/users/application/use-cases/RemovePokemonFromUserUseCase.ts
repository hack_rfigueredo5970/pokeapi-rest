import { UserRepository } from '../../domain/repositories/UserRepository';
import { Pokemon } from '../../../pokemons/domain/entities/interfaces/Pokemon';
import { PokemonRepository } from '../../../pokemons/domain/repositories/PokemonRepository';
import { User } from '../../domain/entities/interfaces/User';
import { NotFoundError } from '../../../errors/errors';

export class RemovePokemonFromUserUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly pokemonRepository: PokemonRepository,
  ) {}

  async execute(userId:string, name:string): Promise< User | null> {
    let pokemon: Pokemon;
    try {
      pokemon = await this.pokemonRepository.findByPokemonName(name) as  Pokemon;
      if (!pokemon) throw new NotFoundError(); 
    } catch (error) {
      console.error((error as Error)?.message);
      return await this.userRepository.findById(userId);
    }
    return this.userRepository.removePokemonFromUser(userId, pokemon?._id || '');
  }

}
