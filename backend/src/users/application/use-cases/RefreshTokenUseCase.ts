import { UnauthorizedError } from '../../../errors/errors';
import { generateAccessToken } from '../services/authentication/AuthenticationService';
import { AuthTokenResponse } from '../../../utils/interfaces/AuthTokenResponse';
import { config } from '../../../config/config';
import jwt from 'jsonwebtoken';

export class RefreshTokenUseCase {
  async execute( refreshToken: string): Promise<AuthTokenResponse> {
        
    // Verify the refresh token
    const decodedRefreshToken = jwt.verify(refreshToken, config?.refreshTokenSecret || '');
    if (!decodedRefreshToken) {
      throw new UnauthorizedError('Invalid refresh token');
    }
    const accessToken = generateAccessToken((decodedRefreshToken as Record<string, string>).userId);


    return { accessToken, refreshToken  };
  }
}

