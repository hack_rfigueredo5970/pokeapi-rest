import { UserRepository } from '../../domain/repositories/UserRepository';
import { Pokemon } from '../../../pokemons/domain/entities/interfaces/Pokemon';
import { PokemonRepository } from '../../../pokemons/domain/repositories/PokemonRepository';
import { CreatePokemonUseCase } from '../../../pokemons/application/use-cases/CreatePokemonUseCase';
import { User } from '../../domain/entities/interfaces/User';

export class AddPokemonToUserUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly pokemonRepository: PokemonRepository,
    private readonly createPokemonUseCase: CreatePokemonUseCase,
  ) {}

  async execute(userId:string, name:string): Promise< User | null> {
    let pokemon: Pokemon = await this.pokemonRepository.findByPokemonName(name) as  Pokemon;
    if (!pokemon) {
      pokemon = await this.createPokemonUseCase.execute(name);
    }
    return this.userRepository.addPokemonToUser(userId, pokemon);
  }
}
