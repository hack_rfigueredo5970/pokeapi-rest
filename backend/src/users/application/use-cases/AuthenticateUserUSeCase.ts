import { InternalServerError, UnauthorizedError } from '../../../errors/errors';
import { UserRepository } from '../../domain/repositories/UserRepository';
import { generateAccessToken, generateRefreshToken } from '../services/authentication/AuthenticationService';
import { User } from '../../domain/entities/interfaces/User';
import { HashingService } from '../services/hashing/interfaces/HashingService';
import { AuthTokenResponse } from '../../../utils/interfaces/AuthTokenResponse';

export class AuthenticateUserUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly hashingService: HashingService,
  ) {}
  
  async execute( username: string,
    password: string): Promise<AuthTokenResponse> {
    const user: User = await this.userRepository.findByUserName(username) as User;
    const isValidPassword: boolean = await this.hashingService.comparePassword(password, user?.password || '');
      
    if (!user?._id || !isValidPassword) {
      throw new UnauthorizedError('Invalid credentials');
    }
    
    // update refreshToken
    let refreshToken  = '';
    try {
      refreshToken = generateRefreshToken(user._id || '');
      const userUpdated: User = await this.userRepository.updateUser(user?._id, { token: refreshToken }) as User;
      if (!userUpdated?._id) {
        throw new InternalServerError('Fail user update');
      }
    } catch (error) {
      console.error(`Error RefreshToken: ${(error as Error).message}`);
    }
  

    const accessToken = generateAccessToken(user._id || '');
    return { accessToken, refreshToken  };
  }
}

