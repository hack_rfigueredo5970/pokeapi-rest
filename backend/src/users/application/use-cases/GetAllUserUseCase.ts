import { UserRepository } from '../../domain/repositories/UserRepository';
import { User } from '../../domain/entities/interfaces/User';

export class GetAllUserUseCase {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(): Promise<User[]> {
    return this.userRepository.getAllUser();
  }
}
