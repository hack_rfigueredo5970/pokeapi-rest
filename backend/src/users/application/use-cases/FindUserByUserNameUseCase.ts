import { UserRepository } from '../../domain/repositories/UserRepository';
import { User } from '../../domain/entities/interfaces/User';

export class FindUserByUserNameUseCase {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(username: string): Promise<User | null> {
    return this.userRepository.findByUserName(username);
  }
}
