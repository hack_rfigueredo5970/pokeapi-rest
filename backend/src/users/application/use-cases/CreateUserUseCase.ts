import { BadRequestError } from '../../../errors/errors';
import { User } from '../../domain/entities/interfaces/User';
import { UserRepository } from '../../domain/repositories/UserRepository';
import { HashingService } from '../services/hashing/interfaces/HashingService';

export class CreateUserUseCase {
  constructor(
    private readonly userRepository: UserRepository, 
    private readonly hashingService: HashingService,
  ) {}

  async execute(user: User): Promise<User> {
    user.password = user?.password?.trim();
    user.username = user?.username?.trim();
    const existingUser = await this.userRepository.findByUserName(user?.username || '');
    if (existingUser) {
      throw new BadRequestError(`username ${user.username } is already in use!`);
    }
    const hashedPassword = await this.hashingService.hashPassword(user?.password || '');

    // Create the user
    const createdUser = await this.userRepository.createUser({ ...user, password:  hashedPassword });

    return createdUser;
  }
}
