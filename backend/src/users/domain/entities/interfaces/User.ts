import { Pokemon } from '../../../../pokemons/domain/entities/interfaces/Pokemon';
import { StringUUID } from '../../../../utils/types/StringUUID';

export interface User {
  _id?: StringUUID,
  username: string;
  password?: string; // Add the user property to associate the task with a user.
  token?: string;
  pokemons?: Pokemon[]; 
}
  