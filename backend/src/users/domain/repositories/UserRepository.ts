import { Pokemon } from '../../../pokemons/domain/entities/interfaces/Pokemon';
import { User } from '../entities/interfaces/User';

export interface UserRepository {
  createUser(user: User): Promise<User>;
  updateUser(id: string, updatedUserData: Partial<User>) : Promise<User | null>; 
  findByUserName(username: string): Promise<User | null>; // Add the findByUserName method
  isUserNameUnique(username: string): Promise<boolean>;
  findById(id: string): Promise<User | null>;
  getAllUser(): Promise<User[]>;
  addPokemonToUser(userId: string, pokemon: Pokemon): Promise<User | null> ;
  getUserPokemons(userId: string): Promise<Pokemon[]>;
  removePokemonFromUser(userId: string, pokemonId: string): Promise<User | null>;
  removePokemonFromAllUsers(pokemonId: string): Promise<void> 
}
