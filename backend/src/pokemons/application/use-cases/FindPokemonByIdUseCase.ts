import { PokemonRepository } from '../../domain/repositories/PokemonRepository';
import { Pokemon } from '../../domain/entities/interfaces/Pokemon';

export class FindPokemonByIdUseCase {
  constructor(private readonly pokemonRepository: PokemonRepository) {}

  async execute(name: string): Promise<Pokemon | null> {
    return this.pokemonRepository.findById(name);
  }
}
