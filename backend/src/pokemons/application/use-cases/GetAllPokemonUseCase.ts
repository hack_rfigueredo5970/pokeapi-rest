import { PokemonRepository } from '../../domain/repositories/PokemonRepository';
import { Pokemon } from '../../domain/entities/interfaces/Pokemon';

export class GetAllPokemonUseCase {
  constructor(private readonly pokemonRepository: PokemonRepository) {}

  async execute(): Promise<Pokemon[]> {
    return this.pokemonRepository.getAllPokemon();
  }
}
