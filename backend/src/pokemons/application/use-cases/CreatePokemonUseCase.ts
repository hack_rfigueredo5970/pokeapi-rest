import { BadRequestError } from '../../../errors/errors';
import { Pokemon } from '../../domain/entities/interfaces/Pokemon';
import { PokemonRepository } from '../../domain/repositories/PokemonRepository';
import { PokemonAPIService } from '../services/interfaces/PokemonAPIService';

export class CreatePokemonUseCase {
  constructor(
    private readonly pokemonRepository: PokemonRepository, 
    private readonly pokemonAPIService: PokemonAPIService,
  ) {}

  async execute(name: string): Promise<Pokemon> {

    if (!name?.trim())
      throw new BadRequestError('Missing Pokemon Name');

    let createdPokemon = await this.pokemonRepository.findByPokemonName(name?.trim() || '');

    if (!createdPokemon) {
      // Fetch Pokémon details from the external API
      const pokemonDetails: Pokemon = await this.pokemonAPIService.getPokemonDetailsByName(name) as Pokemon;
      
      if (!pokemonDetails) {
        // Handle the case when the Pokémon is not found in API
        throw new BadRequestError('Pokemon not found'); 
      }

      createdPokemon = await this.pokemonRepository.createPokemon( pokemonDetails);
    }
  
    return createdPokemon;
  }
}
