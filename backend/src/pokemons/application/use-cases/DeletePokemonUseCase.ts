import { UserRepository } from '../../../users/domain/repositories/UserRepository';
import { PokemonRepository } from '../../domain/repositories/PokemonRepository';
import { Pokemon } from '../../domain/entities/interfaces/Pokemon';

export class DeletePokemonUseCase {
  constructor(
    private readonly pokemonRepository: PokemonRepository,
    private readonly userRepository: UserRepository,
  ) {}

  async execute(name: string): Promise<boolean> {
    const pokemon: Pokemon = await this.pokemonRepository.findByPokemonName(name) as Pokemon;

    if (!pokemon)    
      return true;
    
    await this.userRepository.removePokemonFromAllUsers(pokemon._id || '');  
    return this.pokemonRepository.deletePokemon(pokemon._id || '');
  }
}
