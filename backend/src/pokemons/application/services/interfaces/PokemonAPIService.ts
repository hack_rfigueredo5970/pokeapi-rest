import { Pokemon } from '../../../domain/entities/interfaces/Pokemon';


export interface PokemonAPIService {
  getPokemonDetailsByName(name: string): Promise<Pokemon | null>;
}
