import axios from 'axios';
import { Pokemon } from '../../domain/entities/interfaces/Pokemon';
import { PokemonAPIService } from './interfaces/PokemonAPIService';

export interface PokemonAPITypeAParam { type: { name: string; }; }

export class PokemonAPIServiceAdapter implements PokemonAPIService {
  private readonly baseUrl = 'https://pokeapi.co/api/v2/pokemon';

  async getPokemonDetailsByName(name: string): Promise<Pokemon | null> {
    try {
      const response = await axios.get(`${this.baseUrl}/${name}`);
      const data = response.data;

      return {
        name: data.name,
        img: data.sprites.front_default,
        height: data.height,
        weight: data.weight,
        types: data.types.map((type: PokemonAPITypeAParam) => type.type.name),
      };
    } catch (error) {
      console.error(`PokeApiErrorConnection: ${(error as Error)?.message} `);
    }
    return null;
  }
}
