import { Router } from 'express';
import { initializePokemonController } from '../../dependencies';
import { authenticate } from '../../../../midllewares/authenticationMiddleware';

const initializePokemonRouter = async () => {

  const pokemonController = await initializePokemonController();
  const pokemonRouter = Router();
  pokemonRouter.get('/', authenticate, pokemonController.getAllPokemon.bind(pokemonController));
  pokemonRouter.get('/:pokemonId', authenticate, pokemonController.findById.bind(pokemonController));
  pokemonRouter.get('/:name', authenticate, pokemonController.findByPokemonName.bind(pokemonController));
  pokemonRouter.delete('/:name', authenticate, pokemonController.deleteByPokemonName.bind(pokemonController));
  pokemonRouter.post('/create', authenticate, pokemonController.create.bind(pokemonController));
  return pokemonRouter;
};

export default initializePokemonRouter;
