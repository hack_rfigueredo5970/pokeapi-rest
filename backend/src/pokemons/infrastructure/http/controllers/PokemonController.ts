

// libs
import { BadRequestError, ConflictError, NotFoundError  } from '../../../../errors/errors';
import { Request, Response, NextFunction } from 'express';

// use cases
import { CreatePokemonUseCase } from '../../../application/use-cases/CreatePokemonUseCase';
import { FindPokemonByNameUseCase } from '../../../application/use-cases/FindPokemonByNameUseCase';
import { FindPokemonByIdUseCase } from '../../../application/use-cases/FindPokemonByIdUseCase';
import { GetAllPokemonUseCase } from '../../../application/use-cases/GetAllPokemonUseCase';
import { DeletePokemonUseCase } from '../../../application/use-cases/DeletePokemonUseCase';

// interfaces
import { Pokemon } from '../../../domain/entities/interfaces/Pokemon';



export class PokemonController {
  constructor(
    private readonly createPokemonUseCase: CreatePokemonUseCase,
    private readonly getAllPokemonUseCase: GetAllPokemonUseCase,
    private readonly findPokemonByNameUseCase: FindPokemonByNameUseCase,
    private readonly findPokemonByIdUseCase: FindPokemonByIdUseCase,
    private readonly deletePokemonUseCase: DeletePokemonUseCase,
  ) {}


  async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const { name } = req.body;
    
      // Check if any required attributes are missing
      if (!name) {
        throw new BadRequestError('Missing required field: name');
      }
  
      const createdPokemon = await this.createPokemonUseCase.execute(name);

      res.status(201).json(createdPokemon);
    } catch (error) {
      next(error);
    }
    return;
  }

  async getAllPokemon(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const pokemons : Pokemon[] = await this.getAllPokemonUseCase.execute() || [];
      res.status(200).json({ pokemons });
    } catch (error) {
      next(error);
    }
    return;
  }

  async findByPokemonName(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const { name } = req.params;
      const pokemon : Pokemon | null = await this.findPokemonByNameUseCase.execute(name);
    
    
      if (!pokemon) {
        throw new NotFoundError('Pokemon not found'); 
      }

      res.status(200).json(pokemon);
    } catch (error) {
      next(error);
    }
    return;
  }

  async findById(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const  { pokemonId } = req.body;
      const pokemon : Pokemon | null = await this.findPokemonByIdUseCase.execute(pokemonId);
      
      if (!pokemon) {
        throw new NotFoundError('Pokemon not found'); 
      }

      res.status(200).json(pokemon);
    } catch (error) { 
      next(error);
    }
    return;
  }

  async deleteByPokemonName(req: Request, res: Response, next: NextFunction) : Promise<void> {
    try {
      const { name } = req.params;
      const  isDeleted: boolean = await this.deletePokemonUseCase.execute(name);
    
    
      if (!isDeleted) {
        throw new ConflictError('It was not able to complete the deletion of resource'); 
      }

      res.status(204).json();
    } catch (error) {
      next(error);
    }
    return;
  }
}