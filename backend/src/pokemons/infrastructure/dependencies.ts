import { CreatePokemonUseCase } from '../application/use-cases/CreatePokemonUseCase';
import { FindPokemonByNameUseCase } from '../application/use-cases/FindPokemonByNameUseCase';
import { FindPokemonByIdUseCase } from '../application/use-cases/FindPokemonByIdUseCase';
import { PokemonRepository } from '../domain/repositories/PokemonRepository';
import { PokemonDatabaseFactory } from './databases/PokemonDatabaseFactory';
import { PokemonController } from './http/controllers/PokemonController';

import { GetAllPokemonUseCase } from '../application/use-cases/GetAllPokemonUseCase';
import { PokemonAPIServiceAdapter } from '../application/services/PokemonAPIServiceAdapter';
import { PokemonAPIService } from '../application/services/interfaces/PokemonAPIService';
import { DeletePokemonUseCase } from '../application/use-cases/DeletePokemonUseCase';
import { UserDatabaseFactory } from '../../users/infrastructure/databases/UserDatabaseFactory';
import { UserRepository } from '../../users/domain/repositories/UserRepository';
  
// Create use cases and controllers, injecting the required dependencies
export const initializePokemonController = async (): Promise<PokemonController> => {

  const userRepository: UserRepository = await UserDatabaseFactory.createUserAdapter();

  const pokemonRepository: PokemonRepository = await PokemonDatabaseFactory.createPokemonAdapter();
  const pokemonAPIService: PokemonAPIService = new PokemonAPIServiceAdapter();
  

  // use cases
  const createPokemonUseCase = new CreatePokemonUseCase(pokemonRepository, pokemonAPIService);
  const getAllPokemonUseCase = new GetAllPokemonUseCase(pokemonRepository);
  const findPokemonByNameUseCase = new FindPokemonByNameUseCase(pokemonRepository);
  const findPokemonByIdUseCase = new FindPokemonByIdUseCase(pokemonRepository);
  const deletePokemonUseCase = new DeletePokemonUseCase(pokemonRepository, userRepository);


  return new PokemonController(
    createPokemonUseCase, 
    getAllPokemonUseCase, 
    findPokemonByNameUseCase, 
    findPokemonByIdUseCase,
    deletePokemonUseCase,
  );
};

