import { MongoDBPokemonAdapter } from './adapters/mongo/MongoDBPokemonAdapter';
import { DatabaseType, DatabaseTypeEnum } from '../../../config/types/DatabaseTypeEnum';
import { PokemonRepository } from '../../domain/repositories/PokemonRepository';
import { config } from '../../../config/config';
import { DatabaseConnectionType, connectToDatabase } from '../../../config/db';

class PokemonDatabaseFactory {
  static async createPokemonAdapter(): Promise<PokemonRepository> {
    const databaseType: DatabaseType  =  config.database.type as DatabaseType;
    if (databaseType === DatabaseTypeEnum.MONGODB ) {
      const db: DatabaseConnectionType = await connectToDatabase();
      return new MongoDBPokemonAdapter(db);
    } else {
      throw new Error(`Unsupported database type: ${databaseType}`);
    }
  }
}

export { PokemonDatabaseFactory };
