import { DatabaseConnectionError, DatabaseError } from '../../../../../errors/errors';
import { PokemonRepository } from '../../../../domain/repositories/PokemonRepository';
import { Pokemon } from '../../../../domain/entities/interfaces/Pokemon';
import { Collection, Db, ObjectId, OptionalId } from 'mongodb';
import { DatabaseConnectionType } from '../../../../../config/types/DatabaseConnectionType';
import { PokemonDocument } from './interfaces/PokemonDocument';



class MongoDBPokemonAdapter implements PokemonRepository {

  private collection: Collection;

  constructor(db: DatabaseConnectionType) {
    if (!db) 
      throw new DatabaseConnectionError();
    this.collection = (db as Db).collection('Pokemon');
  }
  
  async createPokemon(pokemon: Pokemon): Promise<Pokemon> {
    try {
      // Create a MongoDB document from the Pokemon object
      const pokemonDoc: OptionalId<PokemonDocument>  = {
        name: pokemon.name,
        height: pokemon.height,
        weight: pokemon.height,
        img: pokemon.img,
        types: pokemon. types,
      };



      const result = await this.collection.insertOne(pokemonDoc);
      if (!result.insertedId) {
        throw new DatabaseError('Failed to insert pokemon');
      }
  
      return (this.collection.findOne({ _id: result.insertedId })  as unknown as Pokemon);
  
    } catch (error) {
      throw new DatabaseError(`Failed to create pokemon: ${(error as Error).message}`);
    }
  }

  async updatePokemon(id: string, updatedPokemonData: Partial<Pokemon>): Promise<Pokemon | null> {
    try {
      const result = await this.collection.updateOne({ _id: new ObjectId(id) }, { $set: updatedPokemonData });
  
      if (result.modifiedCount === 0) {
        return null; // No pokemon was updated
      }
  
      const updatedPokemon = await this.collection.findOne({ _id: new ObjectId(id) });
      return updatedPokemon as unknown as Pokemon;
    } catch (error) {
      throw new DatabaseError(`Failed to update pokemon: ${(error as Error).message}`);
    }
  }


  async getAllPokemon(): Promise<Pokemon[]> {
    try {
      const pokemons = await this.collection.find().toArray();
      return pokemons.map((pokemon) => pokemon as unknown as Pokemon);
    } catch (error) {
      throw new DatabaseError(`Failed to retrieve all pokemons: ${(error as Error).message}`);
    }
  }

  async findByPokemonName(name: string): Promise<Pokemon | null> {
    try {
      return ( await await this.collection.findOne({ name }) as unknown as Pokemon );
    } catch (error) {
      throw new DatabaseError(`Failed to find pokemon by name: ${(error as Error).message}`);
    }
  }

  async isPokemonNameUnique(name: string): Promise<boolean> {
    const pokemon = await this.collection.findOne({ name });
    return !pokemon; // Return true if name is unique (pokemon doesn't exist)
  }

  async findById(id: string): Promise<Pokemon | null> {
    try {
      return (this.collection.findOne({ _id: new ObjectId(id) }) as unknown as Pokemon);
    } catch (error) {
      throw new DatabaseError(`Failed to get pokemon by ID: ${(error as Error).message}`);
    }
  }

  async deletePokemon(id: string): Promise<boolean> {
    try {
      const result = await this.collection.deleteOne({ _id: new ObjectId(id) });

      if (result.deletedCount === 1) {         
        return true; // Pokémon successfully deleted
      } else {
        return false; // Pokémon with the given ID not found
      }
    } catch (error) {
      throw new DatabaseError(`Failed to delete Pokémon: ${(error as Error).message}`);
    }
  }
}

export { MongoDBPokemonAdapter };
