import { Pokemon } from '../../../../../domain/entities/interfaces/Pokemon';


export interface PokemonDocument extends Omit<Pokemon, '_id'> {}

