import { StringUUID } from '../../../../utils/types/StringUUID';

export interface Pokemon {
  _id?: StringUUID,
  name: string;
  height?: string;
  weight?: string; 
  img?: string;
  types?: string[];
}
  