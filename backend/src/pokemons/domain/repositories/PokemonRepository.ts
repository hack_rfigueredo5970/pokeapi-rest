import { Pokemon } from '../entities/interfaces/Pokemon';

export interface PokemonRepository {
  createPokemon(Pokemon: Pokemon): Promise<Pokemon>;
  updatePokemon(id: string, updatedPokemonData: Partial<Pokemon>) : Promise<Pokemon | null>; 
  findByPokemonName(name: string): Promise<Pokemon | null>; 
  isPokemonNameUnique(name: string): Promise<boolean>;
  findById(id: string): Promise<Pokemon | null>;
  getAllPokemon(): Promise<Pokemon[]>;
  deletePokemon(id: string): Promise<boolean>;
}
