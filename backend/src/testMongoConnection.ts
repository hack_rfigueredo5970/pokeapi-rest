// Import the connectToDatabase function from your original file
import { DatabaseConnectionType, connectToDatabase } from './config/db';

// Call the connectToDatabase function to establish the connection
async function testMongoConnection() {
  try {
    const db: DatabaseConnectionType = await connectToDatabase();
    console.log('Connected to MongoDB successfully!');

    const collection = db?.collection('User');


    const result = await collection?.findOne();

    console.log('document found:', result);
  
  } catch (error) {
    console.error('Failed to connect to MongoDB:', (error as Error).message);
  }

  return;
}

// Call the testMongoConnection function
testMongoConnection();
