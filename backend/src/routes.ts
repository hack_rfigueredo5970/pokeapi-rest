import  { Router } from 'express';
import initializeUserRouter from './users/infrastructure/http/routes/UserRoutes';
import initializeUserFavoritesRouter from './users/infrastructure/http/routes/UserFavoritesRoutes';
import initializePokemonRouter from './pokemons/infrastructure/http/routes/PokemonRoutes';



export async function createRoutes(): Promise<Router> {
  const router: Router = Router();

  const UserRoutes = await  initializeUserRouter();
  router.use('/users', UserRoutes);

  const UserFavoritesRoutes = await  initializeUserFavoritesRouter();
  router.use('/user/favorites', UserFavoritesRoutes);


  const   PokemonRoutes = await  initializePokemonRouter();
  router.use('/pokemons', PokemonRoutes);

  return router;
}