# Poke APi REST

## Description

A simple application with a backend API built using Node.js, Express, and TypeScript, and a React frontend runing on Vite.

## Table of Contents

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Running the Application](#running-the-application)
- [Usage](#usage)
- [Backend](#backend)
- [Frontend](#frontend)
- [Author](#author)
- [License](#license)

## Getting Started

### Prerequisites

- [Node.js](https://nodejs.org/) (v18.17.1 (LTS))
- [npm](https://www.npmjs.com/) (v8.19.4)

### Installation

1. Clone the repository:

   ```bash
    git clone https://gitlab.com/hack_rfigueredo5970/rafael-awesome-todo-list.git
    cd rafael-awesome-todo-list

2. Install backend dependencies

    ```bash
     cd backend
     npm install

3. Set env variables in backend from env.example

      ```bash
       cp .env.example .env

    Modify the variables MONGO_DB_URI must be empty for localhost conection 
    or set a mongodb atlas uri. For mongodb atlas uri need to allow the ports connection and wushlit the ip public address.

    DB_NAME is the name of the database 

    DB_TYPE only allows mongodb as value because there aren't any other adapters implemented


3. Restore the DB (You need first set the  env variables)

      ```bash
       npm run reset-mongodb 

4. Install frontend dependencies

      ```bash
       cd ../frontend
       npm install

## Running the Application

1. Start the backend server:

   ```bash
    cd backend 
    npm run dev

The server will be running at <http://localhost:3000>

2. Start the frontend server:

   ```bash
    cd frontend 
    npm run dev

The server will be running at <http://localhost:5173>

## Usage

Login with the default user or sign up a new user. Enter any user of the list and add new pokemons by name.
Also can clik the icon to remove the pokemon from the Db

## Backend

 The backend follows Clean Arquitecture practice through the implementation of an Hexagonal Arquitecture with Domain Driven Design. The authentification use JWT Token. The technology stack is Typescript, NodeJs, and Express. It has an integration with PokeAPI to get detail information of pokemons before store  a new pokemon to the Db.

## Frontend

The frontend is vite project for Rect+Typescript. It follows a simple monorepo arquitecture. It uses the Material UI components and Styles

## Author

Rafael Figueredo - <refo44@gmail.com>

## License

This is an open source project, "license": "ISC"
